let mongoose = require('mongoose');

let photoSchema = new mongoose.Schema({
    path: {
        type: String,
        required: 'photo must have a path!'
    },
    path_sm: {
        type: String,
        required: 'photo must have a path for a smaller version!'
    }
});

let Photo =  mongoose.model('Photo', photoSchema);

module.exports = Photo;