let mongoose = require('mongoose');

let itemContentSchema = new mongoose.Schema({
    nameRUS: {
        type: String,
        required: 'content must have a rus name!'
    },
    percentage: {
        type: Number,
        required: 'content must have a percentage'
    }
});

let ItemContent =  mongoose.model('ItemContent', itemContentSchema);

module.exports = ItemContent;