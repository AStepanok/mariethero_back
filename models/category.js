let mongoose = require('mongoose');

let categorySchema = new mongoose.Schema({
    nameRUS: {
        type: String,
        required: 'Category must have a rus name!'
    },
    previewPhoto: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Photo'
    }
});

let Category = mongoose.model('Category', categorySchema);

module.exports = Category;