let mongoose =  require('mongoose');

let itemSchema = new mongoose.Schema({
    category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Category'
    },
    nameRUS: {
        type: String,
        required: 'item must have a rus name!'
    },
    priceRUB: {
        type: Number,
        required: 'item must have a price!'
    },
    code: {
        type: String,
        required: 'item must have a unique code!',
        unique: true,
        dropDups: true
    },
    creationDate: {
        type: Date,
        default: Date.now
    },
    color: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Color'
    },
    counts: [
        {
            size: {
                type: String,
                required: 'size must have a name!'
            },
            number: {
                type: Number,
                required: 'size must have a qunatitiy number'
            }
        }
    ],
    content: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'itemContent'
        }
    ],
    description: {
        type: String,
        required: 'item must have a description'
    }
});

let Item =  mongoose.model('Item', itemSchema);

module.exports = Item;