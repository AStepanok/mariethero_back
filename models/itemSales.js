let mongoose = require('mongoose');

let itemSalesSchema = new mongoose.Schema({
    itemId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Item'
    },
    date: {
        type: Date,
        required: 'item sales must have a date'
    },
    number: {
        type: Number,
        required: 'item sales must have a sales number'
    }
});

let ItemSales =  mongoose.model('ItemSales', itemSalesSchema);

module.exports = ItemSales;