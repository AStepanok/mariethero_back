let express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    cors = require('cors');

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.get('/', (req, res) => {
   res.json({
       message: 'hi'
   })
});

app.listen(8084, () => {
    console.log('Server is running on 8084 port...')
});