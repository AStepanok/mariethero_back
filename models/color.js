let mongoose = require('mongoose');

let colorSchema = new mongoose.Schema({
    nameRUS: {
        type: String,
        required: 'color must have a rus name!'
    },
    HEXCode: {
        type: String,
        required: 'color must have a unique HEX code',
        unique: true,
        dropDups: true
    }
});

let Color =  mongoose.model('Color', colorSchema);

module.exports = Color;